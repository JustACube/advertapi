<?php

namespace App;

use App\Interfaces\MainCategoryInterface;
use App\Interfaces\SubCategoryInterface;
use App\Transformers\AdvertTransformer;
use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model implements SubCategoryInterface
{
    public $timestamps = false;

    protected $fillable = [
        'name'
    ];

    public function adverts()
    {
        return $this->hasMany(Advert::class, 'subcategory_id');
    }

    public function mainCategory()
    {
        return $this->belongsTo(MainCategory::class);
    }

    //
    public function getMainCategory(): MainCategoryInterface
    {
        return $this->mainCategory()->first();
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function getAdverts()
    {
        $adverts = $this->adverts()->get();
        return fractal($adverts)
            ->collection($adverts)
            ->transformWith(new AdvertTransformer())
            ->toArray();

    }

    public function setMainCategory(MainCategoryInterface $mainCategory)
    {
        $this->mainCategory()->associate($mainCategory);
    }
}
