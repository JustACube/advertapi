<?php

namespace App\Transformers;

use App\Metric;
use League\Fractal\TransformerAbstract;

class MetricTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Metric $metric)
    {
        return [
            'ip_address' => $metric->getIpAddress(),
            'refer' => $metric->getRefer(),
            'user_agent' => $metric->getUserAgent(),
        ];
    }
}
