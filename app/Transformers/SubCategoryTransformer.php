<?php

namespace App\Transformers;

use App\SubCategory;
use League\Fractal\TransformerAbstract;

/**
 * Class SubCategoryTransformer
 * @package App\Transformers
 */
class SubCategoryTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param SubCategory $subCategory
     * @return array
     */
    public function transform(SubCategory $subCategory)
    {
        return [
            'subCategory' => $subCategory->getName(),
        ];
    }
}
