<?php

namespace App\Http\Controllers;

use App\MainCategory;
use App\SubCategory;
use App\Transformers\MainCategoryTransformer;
use App\Transformers\SubCategoryTransformer;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function getMainCategories(){
        $maincatgeroies = MainCategory::all();

        return fractal()
            ->collection($maincatgeroies)
            ->transformWith(new MainCategoryTransformer())
            ->toArray();
    }

    public function getSubcategories(MainCategory $mainCategory)
    {
          return $mainCategory->getCategories();
    }

    public function getAdverts(MainCategory $mainCategory, SubCategory $subCategory)
    {
        return $subCategory->getAdverts();
    }
}
