<?php

namespace App\Http\Controllers;

use App\Advert;
use App\Http\Requests\FilterRequest;
use App\SubCategory;
use App\Transformers\AdvertTransformer;
use Illuminate\Http\Request;

class FilterController extends Controller
{
    public function filter(FilterRequest $request)
    {
        $adverts = Advert::all();
        $adverts = $adverts->sortByDesc('updated_at');
        if ($request->tittle != null) {
            $pattern = $request->tittle;
            $adverts = $adverts->filter(function ($item) use ($pattern) {

                return preg_match("/$pattern/i", $item->tittle);
            });
        }
        if ($request->minPrice != null) {
            $adverts = $adverts->where('price', '>=', $request->minPrice);
        }
        if ($request->maxPrice != null) {
            $adverts = $adverts->where('price', '<=', $request->maxPrice);
        }
        if ($request->mainCategoryId != null) {
            $subCategories = SubCategory::where('main_category_id', $request->mainCategoryId)->value('id');
            $adverts = $adverts->whereIn('subcategory_id', $subCategories);
        }
        if ($request->subCategoryId != null) {
            $adverts = $adverts->where('subcategory_id', $request->subCategoryId);
        }
        if ($request->sortByPriceAsc != null) {
            switch ($request->sortByPriceAsc) {
                case 'true':
                    $adverts = $adverts->sortBy('price');
                    break;
                case 'false':
                    $adverts = $adverts->sortByDesc('price');
                    break;
            }
        }

        return fractal()
            ->collection($adverts)
            ->transformWith(new AdvertTransformer())
            ->toArray();
    }
}
