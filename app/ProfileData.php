<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 31.07.18
 * Time: 19:16
 */

namespace App;


class ProfileData
{
    private $firstName;
    private $lastName;
    private $email;
    private $city;

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function setFirstName(string $fName)
    {
        $this->firstName = $fName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function setLastName(string $lName)
    {
        $this->lastName = $lName;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function setCity(string $city)
    {
        $this->city = $city;
    }
}