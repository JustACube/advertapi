<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 20.07.18
 * Time: 19:32
 */

namespace App\Interfaces;

use Iterator;

interface SubCategoryInterface
{
    public function getAdverts();

    public function getMainCategory(): MainCategoryInterface;

    public function getName(): string;

    public function setName(string $name);

    public function setMainCategory(MainCategoryInterface $mainCategory);
}