<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 20.07.18
 * Time: 19:05
 */

namespace App\Interfaces;


use App\SubCategory;
use App\User;

interface AdvertInterface
{
    public function getInfo(): array;

    public function getMetrics();

    public function getComments();

    public function getRootCategory();

    public function setCategory(SubCategoryInterface $subCategory);

    public function getCategory(): SubCategoryInterface;

    public function getCountInFavorite(): int;

    public function getAuthor(): UserInterface;

    public function setTittle(string $tittle);

    public function getTittle(): string;

    public function setDescription(string $description);

    public function getDescription(): string;

    public function setImage(string $href);

    public function getImage(): string;

    public function setPrice(int $price);

    public function getPrice(): int;

    public function setStatus(bool $status);

    public function getStatus(): bool;

    public function setUser(UserInterface $user);
}