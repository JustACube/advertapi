<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 20.07.18
 * Time: 19:32
 */

namespace App\Interfaces;

use Iterator;

interface MainCategoryInterface
{
    public function getCategories();

    public function getName(): string;

    public function setName(string $name);
}