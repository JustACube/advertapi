<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 20.07.18
 * Time: 19:05
 */

namespace App\Interfaces;


use App\Advert;
use App\User;

interface CommentInterface
{
    public function setText(string $text);

    public function getText(): string;

    public function getAuthor(): UserInterface;

    public function getAdvert(): AdvertInterface;

    public function setUser(UserInterface $user);

    public function setAdvert(AdvertInterface $advert);

    public function getInfo(): array;
}