<?php

namespace App;

use App\Http\Requests\UpdateUserRequest;
use App\Interfaces\UserInterface;
use App\Transformers\AdvertTransformer;
use App\Transformers\UserTransformer;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable implements UserInterface
{
    use Notifiable;

    protected $fillable = [
        'firstName', 'lastName', 'email', 'password', 'city'
    ];


    protected $hidden = [
        'password', 'remember_token',
    ];

    /*
     * Relationships
     */


    public function adverts()
    {
        return $this->hasMany(Advert::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function favorites()
    {
        return $this->belongsToMany(Advert::class, 'favorites')->withTimestamps();
    }


    /*
     * Getters+setters
     */

    public function getInfo(): array
    {
        return fractal()
            ->item($this)
            ->transformWith(new UserTransformer())
            ->toArray();
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function setFirstName(string $fName)
    {
        $this->firstName = $fName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function setLastName(string $lName)
    {
        $this->lastName = $lName;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function setCity(string $city)
    {
        $this->city = $city;
    }

    public function setPassword(string $password)
    {
        $this->password = $password;
    }

    public function getAdverts(): array
    {
        $adverts = $this->adverts()->get();
        return fractal()
            ->collection($adverts)
            ->transformWith(new AdvertTransformer())
            ->toArray();
    }

    public function getFavorites(): array
    {
        $adverts = $this->favorites()->get();
        return fractal()
            ->collection($adverts)
            ->transformWith(new AdvertTransformer())
            ->toArray();
    }

    public function updateProfile(ProfileData $profileData)
    {
        if ($profileData->getFirstName() != '')
            $this->setFirstName($profileData->getFirstName());
        if ($profileData->getLastName() != '')
            $this->setLastName($profileData->getLastName());
        if ($profileData->getEmail() != '')
            $this->setEmail($profileData->getEmail());
        if ($profileData->getCity() != '')
            $this->setCity($profileData->getCity());
    }

    public function changePassword(string $newPassword)
    {
        $this->password=Hash::make($newPassword);
        $this->remember_token = null;
        $id = $this->id;
        if (Cache::has($id)){
            $token = Cache::get($id);
            Cache::forget($token);
            Cache::forget($id);
        }
    }
}
