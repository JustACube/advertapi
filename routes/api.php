<?php

use App\Jobs\SendTestMailJob;
use App\Mail\TestMail;
use Illuminate\Http\Request;
use \GuzzleHttp\Client;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
use PhpAmqpLib\Connection\AMQPStreamConnection;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'v1'], function () {
    Route::get('/filter', 'FilterController@filter');
    Route::group(['prefix' => 'adverts'], function () {
        Route::post('/', 'AdvertController@store')->middleware('myauth');
        Route::get('/', 'AdvertController@index');
        Route::get('/{advert}', 'AdvertController@show')->where('advert', '[0-9]+');
        Route::put('/{advert}', 'AdvertController@update')->middleware('myauth')
            ->where('advert', '[0-9]+');
        Route::delete('/{advert}', 'AdvertController@destroy')->middleware('myauth')
            ->where('advert', '[0-9]+');
        Route::get('/{advert}/metrics', 'MetricController@index')
            ->where('advert', '[0-9]+');

        Route::post('/{advert}', 'AdvertController@favorite')->middleware('myauth')
            ->where('advert', '[0-9]+');

        Route::group(['prefix' => '/{advert}/comments'], function () {
            Route::post('/', 'CommentController@store')->middleware('myauth');
            Route::put('/{comment}', 'CommentController@update')->middleware('myauth')
                ->where('comment', '[0-9]+');
            Route::delete('/{comment}', 'CommentController@destroy')->middleware('myauth')
                ->where('comment', '[0-9]+');
        });
    });
    Route::group(['prefix' => 'categories'], function () {
        Route::get('/', 'CategoriesController@getMainCategories');
        Route::get('/{main_category}', 'CategoriesController@getSubcategories')
            ->where('main_category', '[0-9]+');
        Route::get('/{main_category}/{sub_category}', 'CategoriesController@getAdverts')
            ->where([
                'main_category' => '[0-9]+',
                'sub_category' => '[0-9]+',]);
    });
});
